﻿using System;
using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MandelBrot
{
  public partial class Form1 : Form

  {
    private readonly ColorTranslationHandler _colorHandler = new ColorTranslationHandler();
    private readonly SemaphoreSlim _drawSemaphore = new SemaphoreSlim(1);
    private int max;
    private double offsetX;
    private double offsetY;
    private double scale;

    public Form1()
    {
      InitializeComponent();

      // Vult de kleurendropdown met de verschillende vertalers
      foreach (string name in _colorHandler.GetNames())
      {
        colorDropDown.Items.Add(name);
      }
      // Selecteer de correcte startinstellingen
      colorDropDown.SelectedItem = "Regenboog";
      presetComboBox.SelectedItem = "Basis";

      // Registreer de event handlers voor het figuur
      image.MouseClick += ChangeZoom;
      image.SizeChanged += UpdateSize;

      DrawMandelBrot();
    }

    private void UpdateSize(object sender, EventArgs e)
    {
      // Teken het figuur opnieuw nadat de grootte van het formulier veranderd is
      DrawMandelBrot();
    }

    private async void DrawMandelBrot()
    {
      // We moeten hier een semafoor gebruiken omdat anders de methode continu wordt aangeroepen tijdens het vergroten en verkleinen
      // van het venster. Dit zou er voor zorgen dat er een wachtrij ontstaat en zelfs na het loslaten de methode nog meerdere malen
      // aangeroepen zal worden.
      if (!_drawSemaphore.Wait(0))
      {
        // De Wait(int milisecondsTimeout) methode geeft false terug als de semafoor al bezet is omdat we de timeout op 0ms hebben ingesteld.
        return;
      }

      int gridHeight = image.Height;
      int gridWidth = image.Width;
      // Try om de invoer te controleren
      try
      {
        // Standaard accepteren de conversiemethoden alleen komma's en geen punten
        offsetX = Convert.ToDouble(xMiddleInput.Text.Replace('.', ','));
        offsetY = Convert.ToDouble(yMiddleInput.Text.Replace('.', ','));
        scale = Convert.ToDouble(scaleInput.Text.Replace('.', ','));
        max = Convert.ToInt32(maxInput.Text);
      }
      catch (FormatException)
      {
        // Bij foute invoer geeft het dan een pop up message en stopt de methode
        MessageBox.Show("De invoer is incorrect, probeer het nog een keer");
        return;
      }
      string colorTranslator = colorDropDown.Text;

      // De berekening zelf gebeurt in een aparte thread zodat de GUI niet vastloopt
      Bitmap result = await Task.Run(() =>
      {
        // We tekenen naar een bitmap in het geheugen die vervolgens in een keer op het scherm getekend kan worden.
        // Dit voorkomt onnodige draw calls en maakt de applicatie een stuk sneller.
        Bitmap bitmap = new Bitmap(gridWidth, gridHeight);
        // Om de berekening nog sneller te maken parallelliseren we de berekening.
        // Het gevolg hiervan is wel dat de bitmap gelockt zou moeten worden met een monitor omdat dit een kritische sectie is.
        // Om dit te vermijden schrijven we de berekeningen eerst naar een multidimensionale array.
        Color[,] grid = new Color[gridWidth, gridHeight];

        Parallel.For(0, gridWidth, x =>
        {
          Parallel.For(0, gridHeight, y =>
          {
            int mandelBrot = MathFunctions.CalculateMandelBrot(
              (x - (gridWidth/2))*scale - offsetX,
              -((y - (gridHeight/2))*scale + offsetY), // Y midden moet omgedraaid worden omdat wiskunde
              max
              );
            grid[x, y] = _colorHandler.GetColor(colorTranslator, mandelBrot);
          });
        });

        for (int x = 0; x < gridWidth; x++)
        {
          for (int y = 0; y < gridHeight; y++)
          {
            bitmap.SetPixel(x, y, grid[x, y]);
          }
        }

        return bitmap;
      });

      image.Image = result;

      // Geeft de semafoor weer vrij zodat we opnieuw kunnen tekenen
      _drawSemaphore.Release();
    }

    private void ChangeZoom(object sender, MouseEventArgs e)
    {
      int gridHeight = image.Height;
      int gridWidth = image.Width;

      // Standaard accepteren de conversiemethoden alleen komma's en geen punten
      double offsetX = Convert.ToDouble(xMiddleInput.Text.Replace('.', ','));
      double offsetY = Convert.ToDouble(yMiddleInput.Text.Replace('.', ','));
      double scale = Convert.ToDouble(scaleInput.Text.Replace('.', ','));

      xMiddleInput.Text = (offsetX - ((e.X - gridWidth/2))*scale).ToString();
      yMiddleInput.Text = (offsetY + ((e.Y - gridHeight/2))*scale).ToString();

      // Inzoomen met de linkermuisknop en uitzoomen met de rechter
      switch (e.Button)
      {
        case MouseButtons.Left:
          scaleInput.Text = (scale/2).ToString();
          break;
        case MouseButtons.Right:
          scaleInput.Text = (scale*2).ToString();
          break;
      }
      DrawMandelBrot();
    }

    private void goButton_Click(object sender, EventArgs e)
    {
      DrawMandelBrot();
    }

    private void helpButton_Click(object sender, EventArgs e)
    {
      MessageBox.Show(
        "Linkermuisknop = inzoomen op de geklikte locatie\nRechtermuisknop = uitzoomen op de geklikte locatie\nScroll knop = verplaats middelpunt figuur zonder in te zoomen");
    }

    private void presetComboBox_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (presetComboBox.Text)
      {
        case "Waves":
          xMiddleInput.Text = "-0.3";
          yMiddleInput.Text = "0.025";
          scaleInput.Text = "0.0001";
          maxInput.Text = "255";
          break;
        case "Lightning":
          xMiddleInput.Text = "1.38";
          yMiddleInput.Text = "-0.1";
          scaleInput.Text = "0.0001";
          maxInput.Text = "255";
          break;
        case "In het bos naar boven kijken":
          xMiddleInput.Text = "1.32";
          yMiddleInput.Text = "0.09";
          scaleInput.Text = "0.00005";
          maxInput.Text = "255";
          break;
        case "Spiraal":
          xMiddleInput.Text = "-0.351423782348632";
          yMiddleInput.Text = "0.063866586303711";
          scaleInput.Text = "3.125E-06";
          maxInput.Text = "255";
          break;
        // Wordt aangeroepen bij 'Basis' en bij ongespecificeerde items
        default:
          xMiddleInput.Text = "0";
          yMiddleInput.Text = "0";
          scaleInput.Text = "0.01";
          maxInput.Text = "255";
          break;
      }
    }
  }
}