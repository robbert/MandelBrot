﻿using System.Drawing;

namespace MandelBrot.ColorTranslators
{
  internal interface IColorTranslator
  {
    /// <summary>
    ///   Bevat een leesbare naam voor de vertaler
    /// </summary>
    string FullName { get; }

    /// <summary>
    ///   Berekent een kleur op basis van het mandelbrotgetal
    /// </summary>
    /// <param name="mandelBrot">Het getal op basis waarvan de kleur berekend wordt</param>
    /// <returns></returns>
    Color CalculateColor(int mandelBrot);
  }
}