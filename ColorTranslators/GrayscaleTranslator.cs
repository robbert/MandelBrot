﻿using System;
using System.Drawing;

namespace MandelBrot.ColorTranslators
{
  internal class GrayscaleTranslator : IColorTranslator
  {
    public string FullName { get; } = "Grijwswaarden";

    public Color CalculateColor(int mandelBrot)
    {
      int grayValue = (int) Math.Min(Math.Pow(mandelBrot, 1.5), 255);

      return Color.FromArgb(grayValue, grayValue, grayValue);
    }
  }
}