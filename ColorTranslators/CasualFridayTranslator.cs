﻿using System.Drawing;

namespace MandelBrot.ColorTranslators
{
  internal class CasualFridayTranslator : IColorTranslator
  {
    public string FullName { get; } = "Casual Friday";

    public Color CalculateColor(int mandelBrot)
    {
      return Color.FromArgb(
        (int) ((mandelBrot%8)/8f*255),
        (int) (((mandelBrot*3)%16)/16f*255),
        (int) (((mandelBrot*6)%32)/32f*255)
        );
    }
  }
}