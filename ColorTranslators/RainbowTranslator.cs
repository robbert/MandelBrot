﻿using System.Drawing;

namespace MandelBrot.ColorTranslators
{
  public class RainbowTranslator : IColorTranslator
  {
    public string FullName { get; } = "Regenboog";

    public Color CalculateColor(int mandelBrot)
    {
      switch (mandelBrot%8)
      {
        case 1:
          return Color.Red;
        case 2:
          return Color.Orange;
        case 3:
          return Color.Yellow;
        case 4:
          return Color.Green;
        case 5:
          return Color.DeepSkyBlue;
        case 6:
          return Color.DarkBlue;
        case 7:
          return Color.Purple;
        default:
          return Color.Black;
      }
    }
  }
}