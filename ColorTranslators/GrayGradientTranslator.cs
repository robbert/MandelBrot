﻿using System;
using System.Drawing;

namespace MandelBrot.ColorTranslators
{
  internal class GrayGradientTranslator : IColorTranslator
  {
    public string FullName { get; } = "Grijsovergang";

    public Color CalculateColor(int mandelBrot)
    {
      int grayValue = (int) (Math.Sin(mandelBrot/(40/Math.PI)) * 126f) + 126;

      return Color.FromArgb(grayValue, grayValue, grayValue);
    }
  }
}