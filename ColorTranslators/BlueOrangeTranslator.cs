﻿using System;
using System.Drawing;

namespace MandelBrot.ColorTranslators
{
  internal class BlueOrangeTranslator : IColorTranslator
  {
    public string FullName { get; } = "Blauw en oranje";

    public Color CalculateColor(int mandelBrot)
    {
      int redValue = (int) (Math.Sin(mandelBrot/(30/Math.PI))*126f) + 126;
      int greenValue = (int) (Math.Sin((mandelBrot + 10)/(30/Math.PI))*126f) + 126;
      int blueValue = (int) (Math.Sin((mandelBrot + 20)/(30/Math.PI))*126f) + 126;

      return Color.FromArgb(redValue, greenValue, blueValue);
    }
  }
}