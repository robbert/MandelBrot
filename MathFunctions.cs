﻿using System;

namespace MandelBrot
{
  internal static class MathFunctions
  {
    public static int CalculateMandelBrot(double x, double y, int max)
    {
      double a = 0;
      double b = 0;
      double distance = 0;
      int mandelBrot = 0;

      while (distance < 2 && mandelBrot < max)
      {
        double oldA = a; // Omdat a en b veranderd worden moeten we hun oude waarden opslaan om de nieuwe a en b te kunnen berekenen
        double oldB = b;

        a = oldA*oldA - oldB*oldB + x;
        b = 2*oldA*oldB + y;
        distance = Math.Sqrt(a*a + b*b);

        mandelBrot++;
      }

      return mandelBrot;
    }
  }
}