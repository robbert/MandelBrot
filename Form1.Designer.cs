﻿namespace MandelBrot
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.colorDropDown = new System.Windows.Forms.ComboBox();
            this.presetComboBox = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.maxInput = new System.Windows.Forms.TextBox();
            this.scaleInput = new System.Windows.Forms.TextBox();
            this.yMiddleInput = new System.Windows.Forms.TextBox();
            this.xMiddleInput = new System.Windows.Forms.TextBox();
            this.goButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.image = new System.Windows.Forms.PictureBox();
            this.helpButton = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.image)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.colorDropDown);
            this.panel1.Controls.Add(this.presetComboBox);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.maxInput);
            this.panel1.Controls.Add(this.scaleInput);
            this.panel1.Controls.Add(this.yMiddleInput);
            this.panel1.Controls.Add(this.xMiddleInput);
            this.panel1.Controls.Add(this.goButton);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(60, 8);
            this.panel1.Margin = new System.Windows.Forms.Padding(9, 8, 9, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(400, 80);
            this.panel1.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(256, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Kleurenschema";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(68, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Voorinstelling";
            // 
            // colorDropDown
            // 
            this.colorDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.colorDropDown.FormattingEnabled = true;
            this.colorDropDown.Location = new System.Drawing.Point(202, 57);
            this.colorDropDown.Margin = new System.Windows.Forms.Padding(2);
            this.colorDropDown.Name = "colorDropDown";
            this.colorDropDown.Size = new System.Drawing.Size(190, 21);
            this.colorDropDown.Sorted = true;
            this.colorDropDown.TabIndex = 6;
            // 
            // presetComboBox
            // 
            this.presetComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.presetComboBox.FormattingEnabled = true;
            this.presetComboBox.Items.AddRange(new object[] {
            "Basis",
            "In het bos naar boven kijken",
            "Lightning",
            "Spiraal",
            "Waves"});
            this.presetComboBox.Location = new System.Drawing.Point(6, 57);
            this.presetComboBox.Name = "presetComboBox";
            this.presetComboBox.Size = new System.Drawing.Size(190, 21);
            this.presetComboBox.Sorted = true;
            this.presetComboBox.TabIndex = 5;
            this.presetComboBox.SelectedIndexChanged += new System.EventHandler(this.presetComboBox_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Max";
            // 
            // maxInput
            // 
            this.maxInput.Location = new System.Drawing.Point(249, 20);
            this.maxInput.Name = "maxInput";
            this.maxInput.Size = new System.Drawing.Size(74, 20);
            this.maxInput.TabIndex = 3;
            this.maxInput.Text = "255";
            // 
            // scaleInput
            // 
            this.scaleInput.Location = new System.Drawing.Point(167, 20);
            this.scaleInput.Name = "scaleInput";
            this.scaleInput.Size = new System.Drawing.Size(74, 20);
            this.scaleInput.TabIndex = 2;
            this.scaleInput.Text = "0.01";
            // 
            // yMiddleInput
            // 
            this.yMiddleInput.Location = new System.Drawing.Point(83, 20);
            this.yMiddleInput.Name = "yMiddleInput";
            this.yMiddleInput.Size = new System.Drawing.Size(78, 20);
            this.yMiddleInput.TabIndex = 1;
            this.yMiddleInput.Text = "0";
            // 
            // xMiddleInput
            // 
            this.xMiddleInput.Location = new System.Drawing.Point(6, 20);
            this.xMiddleInput.Name = "xMiddleInput";
            this.xMiddleInput.Size = new System.Drawing.Size(70, 20);
            this.xMiddleInput.TabIndex = 0;
            this.xMiddleInput.Text = "0";
            // 
            // goButton
            // 
            this.goButton.Location = new System.Drawing.Point(329, 18);
            this.goButton.Name = "goButton";
            this.goButton.Size = new System.Drawing.Size(62, 22);
            this.goButton.TabIndex = 4;
            this.goButton.Text = "Teken";
            this.goButton.UseVisualStyleBackColor = true;
            this.goButton.Click += new System.EventHandler(this.goButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(179, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Schaal";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(90, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Y midden";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "X midden";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.image, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.helpButton, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(520, 593);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // image
            // 
            this.image.Dock = System.Windows.Forms.DockStyle.Fill;
            this.image.Location = new System.Drawing.Point(20, 108);
            this.image.Margin = new System.Windows.Forms.Padding(20, 20, 20, 0);
            this.image.Name = "image";
            this.image.Size = new System.Drawing.Size(480, 465);
            this.image.TabIndex = 0;
            this.image.TabStop = false;
            // 
            // helpButton
            // 
            this.helpButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.helpButton.Location = new System.Drawing.Point(20, 573);
            this.helpButton.Margin = new System.Windows.Forms.Padding(20, 0, 20, 0);
            this.helpButton.Name = "helpButton";
            this.helpButton.Size = new System.Drawing.Size(480, 20);
            this.helpButton.TabIndex = 1;
            this.helpButton.Text = "Help";
            this.helpButton.UseVisualStyleBackColor = false;
            this.helpButton.Click += new System.EventHandler(this.helpButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(520, 593);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "MandelBrot SwekApp";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.image)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button goButton;
        private System.Windows.Forms.TextBox xMiddleInput;
        private System.Windows.Forms.TextBox scaleInput;
        private System.Windows.Forms.TextBox yMiddleInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox maxInput;
        private System.Windows.Forms.ComboBox presetComboBox;
    private System.Windows.Forms.ComboBox colorDropDown;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.PictureBox image;
    private System.Windows.Forms.Button helpButton;
  }
}

