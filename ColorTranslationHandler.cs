﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using MandelBrot.ColorTranslators;

namespace MandelBrot
{
  internal class ColorTranslationHandler
  {
    private readonly List<IColorTranslator> _translators = new List<IColorTranslator>();

    public ColorTranslationHandler()
    {
      // Registreer de verschillende vertalers
      _translators.Add(new RainbowTranslator());
      _translators.Add(new GrayscaleTranslator());
      _translators.Add(new CasualFridayTranslator());
      _translators.Add(new GrayGradientTranslator());
      _translators.Add(new BlueOrangeTranslator());
    }

    /// <summary>
    ///   Geeft een lijst met namen van de geregistreerde vertalers
    /// </summary>
    /// <returns>Een array met de namen van de vertalers</returns>
    public string[] GetNames()
    {
      return _translators.Select(t => t.FullName).ToArray();
    }

    /// <summary>
    ///   Geeft een kleur op basis van een translator en een mandelbrotgetal
    /// </summary>
    /// <param name="translator">De volledige naam van de translator</param>
    /// <param name="mandelBrot">Het mandelbrotgetal</param>
    /// <returns>De kleur</returns>
    public Color GetColor(string translator, int mandelBrot)
    {
      return GetTranslator(translator).CalculateColor(mandelBrot);
    }

    /// <summary>
    ///   Geeft een brush op basis van een translator en een mandelbrotgetal
    /// </summary>
    /// <param name="translator">De volledige naam van de translator</param>
    /// <param name="mandelBrot">Het mandelbrotgetal</param>
    /// <returns>De brush</returns>
    public Brush GetBrush(string translator, int mandelBrot)
    {
      return new SolidBrush(GetColor(translator, mandelBrot));
    }

    /// <summary>
    ///   Geeft de vertaler terug op basis van een naam
    /// </summary>
    /// <param name="fullName">De volledige naam van de translator</param>
    /// <returns>De vertaler</returns>
    private IColorTranslator GetTranslator(string fullName)
    {
      return _translators.Find(t => t.FullName == fullName);
    }
  }
}